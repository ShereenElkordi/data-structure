package eg.edu.alexu.csd.datastructure.queue.cs33;

import java.util.LinkedList;

import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

/**
 * @author omid
 *
 */
public class LinkedListBasedImp implements IQueue, ILinkedBased {

	LinkedList<Object> queue = new LinkedList<Object>();

	public void enqueue(Object item) {
		queue.addLast(item);
	}

	public Object dequeue() {
		if(queue.isEmpty()){
			throw new RuntimeException();
		}
		return queue.removeFirst();
	}

	public boolean isEmpty() {
		return queue.isEmpty();
	}

	public int size() {
		int size = 0;
		if (queue.isEmpty()) {
			return size;
		}
		return queue.size();
	}
}
