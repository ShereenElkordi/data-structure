package eg.edu.alexu.csd.datastructure.queue.cs33;

import org.junit.Assert;

import org.junit.Test;

/**
 * @author omid
 *
 */
public class TestingQueue {

	@Test
	public void test() {
		LinkedListBasedImp lq = new LinkedListBasedImp();
		lq.enqueue(0);
		lq.enqueue(1);
		lq.enqueue(2);
		lq.enqueue(3);
		lq.enqueue(4);
		Assert.assertEquals(5, lq.size());
	}

	@Test
	public void test2() {
		LinkedListBasedImp lq = new LinkedListBasedImp();
		lq.enqueue(0);
		lq.enqueue(1);
		lq.enqueue(2);
		lq.enqueue(3);
		Assert.assertEquals(0, lq.dequeue());
		Assert.assertEquals(1, lq.dequeue());
		Assert.assertEquals(2, lq.dequeue());
		Assert.assertEquals(3, lq.dequeue());
	}

	@Test
	public void test3() {
		LinkedListBasedImp lq = new LinkedListBasedImp();
		lq.enqueue(0);
		lq.enqueue(1);
		lq.enqueue(2);
		lq.enqueue(3);
		lq.dequeue();
		lq.dequeue();
		lq.dequeue();
		lq.dequeue();

		Assert.assertEquals(true, lq.isEmpty());
	}

	@Test(expected = RuntimeException.class)
	public void test4() {
		LinkedListBasedImp lq = new LinkedListBasedImp();
		lq.dequeue();
	}

	@Test
	public void test5() {
		ArrayBasedImp aq = new ArrayBasedImp(3);
		aq.enqueue(0);
		aq.enqueue(1);
		aq.enqueue(2);
		Assert.assertEquals(0, aq.dequeue());
		Assert.assertEquals(1, aq.dequeue());
		Assert.assertEquals(2, aq.dequeue());
		Assert.assertEquals(true, aq.isEmpty());

	}

	@Test
	public void test6() {
		ArrayBasedImp aq = new ArrayBasedImp(3);
		aq.enqueue(0);
		aq.enqueue(1);
		Assert.assertEquals(2, aq.size());
	}

	@Test(expected = RuntimeException.class)
	public void test7() {
		ArrayBasedImp aq = new ArrayBasedImp(3);
		aq.enqueue(0);
		aq.enqueue(1);
		aq.enqueue(2);
		aq.enqueue(3);
	}

	@Test(expected = RuntimeException.class)
	public void test8() {
		ArrayBasedImp aq = new ArrayBasedImp(3);
		aq.enqueue(0);
		aq.dequeue();
		aq.dequeue();
	}
}
