package eg.edu.alexu.csd.datastructure.queue.cs33;

import eg.edu.alexu.csd.datastructure.queue.IArrayBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

/**
 * @author omid
 *
 */
public class ArrayBasedImp implements IQueue, IArrayBased {
	Object[] queue = {};
	int size;

	public ArrayBasedImp(int size) {
		queue = new Object[size];
		this.size = size;
	}

	public int first = -1;
	public int last = -1;
	public int counter = 0;
	int one = 1;

	public void enqueue(Object item) {
		last = (last + one) % queue.length;
		counter++;
		if (counter > size) {
			throw new RuntimeException();
		}
		queue[last] = item;
	}

	public Object dequeue() {
		int zero = 0;
		int minusOne = -1;
		first = (first + one) % queue.length;
		if (counter == zero) {
			throw new RuntimeException();
		}
		Object element = queue[first];
		counter--;
		if (first == last) {
			// reset the queue
			first = minusOne;
			last = minusOne;
		}
		return element;
	}

	public boolean isEmpty() {
		
		if (counter==0) {
			return true;
		}
		return false;
	}

	public int size() {
		return counter;
	}

}
