package eg.edu.alexu.csd.datastructure.maze.cs33;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author omid
 *
 */
public class ReadFile {
	public char[][] ReadInput(java.io.File maze) throws IOException {

		if (maze == null) {
			throw new RuntimeException();
		}
		BufferedReader br = new BufferedReader(new FileReader(maze));

		String CurrentLine;
		CurrentLine = br.readLine();
		if (CurrentLine == null) {
			br.close();
			throw new RuntimeException();
		}
		int[] mazeSize = new int[2];
		int j = 0;
		StringBuilder exp = new StringBuilder(CurrentLine);
		StringBuilder currentChar = new StringBuilder(CurrentLine.length());
		for (int i = 0; i < exp.length(); i++) {
			if (exp.charAt(i) != ' ') {
				currentChar.append(exp.charAt(i));
			}
			if (exp.charAt(i) == ' ') {
				if (currentChar.length() != 0 && j < 2) {
					mazeSize[j] = Integer.parseInt(currentChar.toString());
					j++;
					currentChar.setLength(0); // or = new string builder
				}
			}
		}
		if (currentChar.length() != 0) {
			mazeSize[j] = Integer.parseInt(currentChar.toString());
		}
		exp.setLength(0);
		int i = 0;
		j = 0;
		char[][] theMaze = new char[mazeSize[0]][mazeSize[1]];
		while ((CurrentLine = br.readLine()) != null) {
			if (CurrentLine.length() != mazeSize[1] || i >= mazeSize[0]) {
				br.close();
				throw new RuntimeException();
			}
			exp.append(CurrentLine);
			for (j = 0; j < mazeSize[1]; j++) {
				theMaze[i][j] = exp.charAt(j);
			}
			i++;
			exp.setLength(0);
		}
		br.close();

		return theMaze;
	}
}
