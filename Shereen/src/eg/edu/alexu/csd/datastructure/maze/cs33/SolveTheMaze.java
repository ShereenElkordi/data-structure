package eg.edu.alexu.csd.datastructure.maze.cs33;

import java.io.IOException;

import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;
import eg.edu.alexu.csd.datastructure.queue.cs33.LinkedListBasedImp;
import eg.edu.alexu.csd.datastructure.stack.cs33.StackImp;

/**
 * @author omid
 *
 */
public class SolveTheMaze implements IMazeSolver {
	public int[][] solveDFS(java.io.File maze) {
		StackImp path = new StackImp();
		ReadFile obj = new ReadFile();
		int x = 0;
		int y = 0;
		int SFound = 0;
		int EFound = 0;
		char[][] theMaze = null;
		try {
			theMaze = obj.ReadInput(maze);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int numRow = theMaze.length;
		int numCol = theMaze[0].length;
		boolean[][] visited = new boolean[numRow][numCol];
		/// loop to make sure there is only one start and one end
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (theMaze[i][j] == 'S') {
					x = i;
					y = j;
					SFound++;
				}
				if (theMaze[i][j] == 'E') {
					EFound++;
				}
			}
		}
		if (SFound != 1 || EFound != 1) {
			throw new RuntimeException();
		}
		int flag = 0;
		StackImp stack = new StackImp();
		Node current = new Node(x, y, null);
		if (SFound == 1 && EFound == 1) {
			stack.push(current);
			visited[x][y] = true;
			while (!stack.isEmpty()) {
				current = (Node) stack.pop();
				visited[current.x][current.y] = true;
				if (theMaze[current.x][current.y] == 'E') {
					flag = 1;
					break;
				}
				if ((current.x) < numRow && (current.y) + 1 < numCol && (current.x) >= 0 && (current.y) + 1 >= 0
						&& visited[(current.x)][(current.y) + 1] != true
						&& theMaze[(current.x)][(current.y) + 1] != '#') {
					Node temp = new Node((current.x), (current.y) + 1, current);
					stack.push(temp);
				}
				if ((current.x) < numRow && (current.y) - 1 < numCol && (current.x) >= 0 && (current.y) - 1 >= 0
						&& visited[(current.x)][(current.y) - 1] != true
						&& theMaze[(current.x)][(current.y) - 1] != '#') {
					Node temp = new Node((current.x), (current.y) - 1, current);
					stack.push(temp);
				}
				if ((current.x) + 1 < numRow && (current.y) < numCol && (current.x) + 1 >= 0 && (current.y) >= 0
						&& visited[(current.x) + 1][(current.y)] != true
						&& theMaze[(current.x) + 1][(current.y)] != '#') {
					Node temp = new Node((current.x) + 1, (current.y), current);
					stack.push(temp);
				}
				if ((current.x) - 1 < numRow && (current.x) - 1 >= 0 && (current.y) < numCol && (current.y) >= 0
						&& visited[(current.x) - 1][(current.y)] != true
						&& theMaze[(current.x) - 1][(current.y)] != '#') {
					Node temp = new Node((current.x) - 1, (current.y), current);
					stack.push(temp);
				}
			}
		}
		if (flag == 0) {
			return null;
		}
		while (current.getparent() != null) {
			path.push(current);
			current = current.getparent();
		}
		path.push(current);
		StackImp revpath = new StackImp();
		while (!path.isEmpty()) {
			revpath.push(path.pop());
		}
		int[][] result = new int[revpath.size()][2];
		for (int i = revpath.size() - 1; i >= 0; i--) {
			Node tmp = (Node) revpath.pop();
			result[i][0] = tmp.x;
			result[i][1] = tmp.y;
		}
		return result;
	}

	public int[][] solveBFS(java.io.File maze) {
		LinkedListBasedImp path = new LinkedListBasedImp();
		ReadFile obj = new ReadFile();
		int x = 0;
		int y = 0;
		int SFound = 0;
		int EFound = 0;
		char[][] theMaze = null;
		try {
			theMaze = obj.ReadInput(maze);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int numRow = theMaze.length;
		int numCol = theMaze[0].length;
		boolean[][] visited = new boolean[numRow][numCol];
		/// loop to make sure there is only one start and one end
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (theMaze[i][j] == 'S') {
					x = i;
					y = j;
					SFound++;
				}
				if (theMaze[i][j] == 'E') {
					EFound++;
				}
			}
		}
		int flag = 0;
		if (SFound != 1 || EFound != 1) {
			throw new RuntimeException();
		}
		LinkedListBasedImp queue = new LinkedListBasedImp();
		Node current = new Node(x, y, null);
		if (SFound == 1 && EFound == 1) {
			queue.enqueue(current);
			visited[x][y] = true;
			while (!queue.isEmpty()) {
				current = (Node) queue.dequeue();
				visited[current.x][current.y] = true;
				if (theMaze[current.x][current.y] == 'E') {
					flag = 1;
					break;
				}
				if ((current.x) + 1 < numRow && (current.y) < numCol && (current.x) + 1 >= 0 && (current.y) >= 0
						&& visited[(current.x) + 1][(current.y)] != true
						&& theMaze[(current.x) + 1][(current.y)] != '#') {
					Node temp = new Node((current.x) + 1, (current.y), current);
					queue.enqueue(temp);
				}
				if ((current.x) - 1 < numRow && (current.y) < numCol && (current.x) - 1 >= 0 && (current.y) >= 0
						&& visited[(current.x) - 1][(current.y)] != true
						&& theMaze[(current.x) - 1][(current.y)] != '#') {
					Node temp = new Node((current.x) - 1, (current.y), current);
					queue.enqueue(temp);
				}
				if ((current.x) < numRow && (current.y) - 1 < numCol && (current.x) >= 0 && (current.y) - 1 >= 0
						&& visited[(current.x)][(current.y) - 1] != true
						&& theMaze[(current.x)][(current.y) - 1] != '#') {
					Node temp = new Node((current.x), (current.y) - 1, current);
					queue.enqueue(temp);
				}
				if ((current.x) < numRow && (current.x) >= 0 && (current.y) + 1 < numCol && (current.y) + 1 >= 0
						&& visited[(current.x)][(current.y) + 1] != true
						&& theMaze[(current.x)][(current.y) + 1] != '#') {
					Node temp = new Node((current.x), (current.y) + 1, current);
					queue.enqueue(temp);
				}
			}
		}
		if (flag == 0) {
			return null;
		}
		while (current.getparent() != null) {
			path.enqueue(current);
			current = current.getparent();
		}
		path.enqueue(current);
		int[][] result = new int[path.size()][2];
		for (int i = path.size() - 1; i >= 0; i--) {
			Node tmp = (Node) path.dequeue();
			result[i][0] = tmp.x;
			result[i][1] = tmp.y;
		}

		return result;
	}
}
