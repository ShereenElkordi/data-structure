package eg.edu.alexu.csd.datastructure.maze.cs33;

/**
 * @author omid
 *
 */
public class Node {
	public int x;
	public int y;
	public Node parent;

	public Node(int x, int y, Node parent) {
		this.x = x;
		this.y = y;
		this.parent = parent;
	}

	public Node getparent() {
		return this.parent;
	}

}
