package eg.edu.alexu.csd.datastructure.hangman.cs33;

import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class HangmanGame {

	public static void main(String[] args) {
		List<String> listTemp = new ArrayList<String>();
		String line;
		int maxWrongGuesses = 10;
		String word = "";

		BufferedReader br = null;
		FileReader fr;
		try {
			fr = new FileReader("words.txt");
			br = new BufferedReader(fr);
			while ((line = br.readLine()) != null) {
				listTemp.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] array = new String[listTemp.size()];
		array = listTemp.toArray(array);
		functions obj = new functions();
		obj.setDictionary(array);
		obj.setMaxWrongGuesses(maxWrongGuesses);
		obj.selectRandomSecretWord();

		Scanner s = new Scanner(System.in);

		while (!(obj.winCheck())) {
			word = obj.guess(s.next().charAt(0));
			if (word == null) {
				s.close();
				return;
			} else {
				System.out.println(word);
			}

		}
		s.close();
		return;
	}
}
