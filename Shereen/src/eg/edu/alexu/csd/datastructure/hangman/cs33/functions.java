package eg.edu.alexu.csd.datastructure.hangman.cs33;

import java.util.Random;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;

public class functions implements IHangman {

	private String secretWord;
	private String[] words = {};
	private String gameWord;
	int counter = 0;
	int max;

	public void setDictionary(String[] words) {
		this.words = words;
	}

	public String selectRandomSecretWord() {
		Random r = new Random();
		if (words.length == 0) {
			return null;
		}
		secretWord = words[r.nextInt(words.length)];
		gameWord = secretWord;
		if (gameWord == null) {
			System.exit(0);
		}
		StringBuilder sb = new StringBuilder(gameWord);
		for (int i = 0; i < gameWord.length(); i++) {
			sb.setCharAt(i, '-');
		}
		gameWord = sb.toString();

		return secretWord;
	}

	public void setMaxWrongGuesses(Integer max) {
		if (max == null) {
			this.max = 0;
		} else {
			this.max = max;
		}
	}

	public String guess(Character c) {
		if (gameWord.equals(secretWord)) {
			return null;
		}
		Character k = 'c';
		Character tempChar;
		String tempWord;
		if (c == null) {
			return gameWord;
		}
		tempWord = gameWord;
		if (Character.isLowerCase(c)) {
			k = Character.toUpperCase(c);
		}
		if (Character.isUpperCase(c)) {
			k = Character.toLowerCase(c);
		}
		for (int i = 0; i < gameWord.length(); i++) {
			tempChar = secretWord.charAt(i);
			if (tempChar == c || tempChar == k) {
				StringBuilder tempo = new StringBuilder(gameWord);
				tempo.setCharAt(i, tempChar);
				gameWord = tempo.toString();
			}
		}
		if (gameWord == tempWord) {
			counter++;
			if (counter == max) {
				return null;
			}
		}

		return gameWord;
	}

	public boolean winCheck() {
		if (gameWord.equals(secretWord)) {
			return true;
		} else {
			return false;
		}
	}
}
