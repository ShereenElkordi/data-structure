package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;


import org.junit.Assert;
import org.junit.Test;

/**
 * @author omid
 *
 */
public class JUnit_Test7 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();;
		sl.add(2);
		sl.add(3);
		sl.add(4);
		sl.add(5);
		sl.add(6);
		Assert.assertTrue(sl.contains(5));
		Assert.assertFalse(sl.contains(20));
		
		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add(2);
		dl.add(3);
		dl.add(4);
		dl.add(5);
		dl.add(6);
		Assert.assertTrue(dl.contains(5));
		Assert.assertFalse(dl.contains(20));
	}

}
