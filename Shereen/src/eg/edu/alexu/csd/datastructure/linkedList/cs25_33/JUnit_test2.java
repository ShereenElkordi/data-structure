package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author omid
 *
 */
public class JUnit_test2 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();
		sl.add(0);
		sl.add(1);
		sl.add(2, 2);
		sl.add(3, "l");
		sl.add("H");
		sl.add(5, "Data");
		Assert.assertEquals(getClass(), sl.get(10));

		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add(0);
		dl.add(1);
		dl.add(2, 2);
		dl.add(3, "l");
		dl.add("H");
		dl.add(5, "Data");
		Assert.assertEquals(getClass(), dl.get(10));
	}

}
