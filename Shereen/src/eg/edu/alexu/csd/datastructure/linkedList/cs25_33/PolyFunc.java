package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import eg.edu.alexu.csd.datastructure.linkedList.IPolynomialSolver;

/**
 * @author omid
 *
 */
public class PolyFunc implements IPolynomialSolver {
	public SinglyLinkedList A = new SinglyLinkedList();
	public SinglyLinkedList B = new SinglyLinkedList();
	public SinglyLinkedList C = new SinglyLinkedList();
	public SinglyLinkedList R = new SinglyLinkedList();

	public int[][] reverse(int[][] result) {
		int[][] reversedArray = new int[result.length][2];
		int j = result.length - 1;
		// i<result.length "reverse in another array"
		for (int i = 0; i < result.length; i++, j--) {
			reversedArray[i][0] = result[j][0];
			reversedArray[i][1] = result[j][1];
		}
		return reversedArray;
	}

	public void setPolynomial(char poly, int[][] terms) {
		int i = 0;
		int j = 0;
		int k = 1;
		SinglyLinkedList temp = new SinglyLinkedList();
		switch (poly) {
		case 'A':
			A = temp;
			break;
		case 'B':
			B = temp;
			break;
		case 'C':
			C = temp;
			break;
		// polynomial R
		case 'R':
			R = temp;
			break;
		default:
			throw new RuntimeException();
		}
		if (!temp.isEmpty()) {
			temp.clear();
		}
		// adding head
		if(terms[i][k]<0){throw new RuntimeException();} 
		temp.add(j, terms[i][j]);
		i++;
		while (i < terms.length) {
			// if there are a missing exponents like 3 then 1 we add 0 as
			// coefficient
			if (terms[i][k] < 0)
				throw new RuntimeException();
			if (terms[i][k] != (terms[i - k][k] - k)) {
				int newexp = terms[i][k];
				int oldexp = terms[i - k][k];
				if (newexp >= oldexp - k)
					throw new RuntimeException();
				while (newexp < oldexp - k) {
					temp.add(j, j);
					newexp++;
				}
				temp.add(j, terms[i][j]);
				i++;
			} else {
				temp.add(j, terms[i][j]);
				i++;
			}
		}
		// if the last element not equal zero
		if (terms[i - k][k] != 0) {
			int newexp = 0;
			int oldexp = terms[i - k][k];
			while (newexp < oldexp - k) {
				temp.add(j, j);
				newexp++;
			}
			temp.add(j, j);
		}

	}

	public String print(char poly) {
		SinglyLinkedList temp = new SinglyLinkedList();
		switch (poly) {
		case 'A':
			temp = A;
			break;
		case 'B':
			temp = B;
			break;
		case 'C':
			temp = C;
			break;
		// polynomial R
		case 'R':
			temp = R;
			break;
		default:
			throw new RuntimeException();
		}
		if (temp == null) {
			throw new RuntimeException();
		}
		String form=null;
		for (int i = temp.size() - 1; i >= 0; i--) {
			if ((Integer) temp.get(i) != 1 && i != 1) {
				if (i == temp.size() - 1 && (Integer) temp.get(i) != 0)
					form = Integer.toString((Integer) temp.get(i)) + "x^" + Integer.toString(i);
				else if (i == 0 && (Integer) temp.get(i) > 0) {
					form = form + "+" + Integer.toString((Integer) temp.get(i));
				} else if (i == 0 && (Integer) temp.get(i) < 0) {
					form = form + Integer.toString((Integer) temp.get(i));
				} else if ((Integer) temp.get(i) < 0) {
					form = form + Integer.toString((Integer) temp.get(i)) + "x^" + Integer.toString(i);
				} else if ((Integer) temp.get(i) > 0) {
					form = form + "+" + Integer.toString((Integer) temp.get(i)) + "x^" + Integer.toString(i);
				}
			} else if ((Integer) temp.get(i) == 1 && i != 1) {
				if (i == temp.size() - 1 && (Integer) temp.get(i) != 0)
					form = "x^" + Integer.toString(i);
				else if (i == 0 && (Integer) temp.get(i) > 0) {
					form = form + "+" + Integer.toString((Integer) temp.get(i));
				} else if (i == 0 && (Integer) temp.get(i) < 0) {
					form = form + Integer.toString((Integer) temp.get(i));
				} else if ((Integer) temp.get(i) < 0) {
					form = form + "x^" + Integer.toString(i);
				} else if ((Integer) temp.get(i) > 0) {
					form = form + "+" + "x^" + Integer.toString(i);
				}
			}
			if ((Integer) temp.get(i) != 1 && i == 1) {
				if (i == temp.size() - 1 && (Integer) temp.get(i) != 0)
					form = Integer.toString((Integer) temp.get(i)) + "x";
				else if (i == 0 && (Integer) temp.get(i) > 0) {
					form = form + "+" + Integer.toString((Integer) temp.get(i));
				} else if (i == 0 && (Integer) temp.get(i) < 0) {
					form = form + Integer.toString((Integer) temp.get(i));
				} else if ((Integer) temp.get(i) < 0) {
					form = form + Integer.toString((Integer) temp.get(i)) + "x";
				} else if ((Integer) temp.get(i) > 0) {
					form = form + "+" + Integer.toString((Integer) temp.get(i)) + "x";
				}
			} else if ((Integer) temp.get(i) == 1 && i == 1) {
				if (i == temp.size() - 1 && (Integer) temp.get(i) != 0)
					form = "x";
				else if (i == 0 && (Integer) temp.get(i) > 0) {
					form = form + "+" + Integer.toString((Integer) temp.get(i));
				} else if (i == 0 && (Integer) temp.get(i) < 0) {
					form = form + Integer.toString((Integer) temp.get(i));
				} else if ((Integer) temp.get(i) < 0) {
					form = form + "x";
				} else if ((Integer) temp.get(i) > 0) {
					form = form + "+" + "x";
				}
			}
		}
		return form;
	}

	public void clearPolynomial(char poly) {
		switch (poly) {
		case 'A':
			A.clear();
			break;
		case 'B':
			B.clear();
			break;
		case 'C':
			C.clear();
			break;
		case 'R':
			if (R == null) {
				throw new RuntimeException();
			}
			R.clear();
			break;
		default:
			throw new RuntimeException();
		}

	}

	public float evaluatePolynomial(char poly, float value) {
		SinglyLinkedList temp = new SinglyLinkedList();
		switch (poly) {
		case 'A':
			if (A == null) {
				throw new RuntimeException();
			}
			temp = A;
			break;
		case 'B':
			if (B == null) {
				throw new RuntimeException();
			}
			temp = B;
			break;
		case 'C':
			if (C == null) {
				throw new RuntimeException();
			}
			temp = C;
			break;
		// polynomial R
		case 'R':
			if (R == null) {
				throw new RuntimeException();
			}
			temp = R;
			break;
		default:
			throw new RuntimeException();
		}

		float ans = 0;
		int n = temp.size();
		int i = 0;
		for (i = 0; i < n; i++) {
			ans = ans + (float) (Integer) temp.get(i) * (float) Math.pow((double) value, (double) i);
		}
		return ans;
	}

	public int[][] add(char poly1, char poly2) {
		SinglyLinkedList first = new SinglyLinkedList();
		SinglyLinkedList second = new SinglyLinkedList();
		if (!R.isEmpty()) {
			R.clear();
		}
		switch (poly1) {
		case 'A':
			first = A;
			break;
		case 'B':
			first = B;
			break;
		case 'C':
			first = C;
			break;
		default:
			throw new RuntimeException();
		}
		switch (poly2) {
		case 'A':
			second = A;
			break;
		case 'B':
			second = B;
			break;
		case 'C':
			second = C;
			break;
		default:
			throw new RuntimeException();
		}
		if (first == null) {
			throw new RuntimeException();
		}
		if (second == null) {
			throw new RuntimeException();
		}
		A.print();
		System.out.println("");
		B.print();
		ListNode i = first.head;
		ListNode j = second.head;
		if (first.size() > second.size()) {
			for (int k = 0; k < first.size(); k++) {
				if (i != null && j != null) {
					R.add((Integer) (first.get(k)) + (Integer) (second.get(k)));
					i = i.next;
					j = j.next;
				} else if (j == null && i != null) {
					R.add((Integer) (first.get(k)));
					i = i.next;
				}
			}
		} else if (first.size() < second.size()) {
			for (int k = 0; k < second.size(); k++) {
				if (i != null && j != null) {
					R.add((Integer) (first.get(k)) + (Integer) (second.get(k)));
					i = i.next;
					j = j.next;
				} else if (i == null && j != null) {
					R.add((Integer) (first.get(k)));
					j = j.next;
				}
			}
		} else if (first.size() == second.size()) {
			for (int k = 0; k < second.size(); k++) {
				if (i != null && j != null) {
					R.add((Integer) (first.get(k)) + (Integer) (second.get(k)));
					i = i.next;
					j = j.next;
				}
			}
		}
		int[][] result = new int[R.size()][2];
		for (int k = 0; k < R.size(); k++) {
			result[k][0] = (Integer) R.get(k);
			result[k][1] = k;
		}
		result = reverse(result);
		return result;
	}

	public int[][] subtract(char poly1, char poly2) {
		SinglyLinkedList first = new SinglyLinkedList();
		SinglyLinkedList second = new SinglyLinkedList();
		if (!R.isEmpty()) {
			R.clear();
		}
		switch (poly1) {
		case 'A':
			first = A;
			break;
		case 'B':
			first = B;
			break;
		case 'C':
			first = C;
			break;
		default:
			throw new RuntimeException();
		}
		switch (poly2) {
		case 'A':
			second = A;
			break;
		case 'B':
			second = B;
			break;
		case 'C':
			second = C;
			break;
		default:
			throw new RuntimeException();
		}
		if (first == null) {
			throw new RuntimeException();
		}
		if (second == null) {
			throw new RuntimeException();
		}
		A.print();
		System.out.println("");
		B.print();
		ListNode i = first.head;
		ListNode j = second.head;
		if (first.size() > second.size()) {
			for (int k = 0; k < first.size(); k++) {
				if (i != null && j != null) {
					R.add((Integer) (first.get(k)) - (Integer) (second.get(k)));
					i = i.next;
					j = j.next;
				} else if (j == null && i != null) {
					R.add((Integer) (first.get(k)));
					i = i.next;
				}
			}
		} else if (first.size() < second.size()) {
			for (int k = 0; k < second.size(); k++) {
				if (i != null && j != null) {
					R.add((Integer) (first.get(k)) - (Integer) (second.get(k)));
					i = i.next;
					j = j.next;
				} else if (i == null && j != null) {
					R.add(-(Integer) (first.get(k)));
					j = j.next;
				}
			}
		} else if (first.size() == second.size()) {
			for (int k = 0; k < second.size(); k++) {
				if (i != null && j != null) {
					R.add((Integer) (first.get(k)) - (Integer) (second.get(k)));
					i = i.next;
					j = j.next;
				}
			}
		}
		int[][] result = new int[R.size()][2];
		for (int k = 0; k < R.size(); k++) {
			result[k][0] = (Integer) R.get(k);
			result[k][1] = k;
		}
		result = reverse(result);
		setPolynomial('R', result);
		return result;
	}

	public int[][] multiply(char poly1, char poly2) {
		SinglyLinkedList first = new SinglyLinkedList();
		SinglyLinkedList second = new SinglyLinkedList();
		SinglyLinkedList x = new SinglyLinkedList();
		SinglyLinkedList h = new SinglyLinkedList();
		if (!R.isEmpty()) {
			R.clear();
		}
		switch (poly1) {
		case 'A':
			first = A;
			break;
		case 'B':
			first = B;
			break;
		case 'C':
			first = C;
			break;
		default:
			throw new RuntimeException();
		}
		switch (poly2) {
		case 'A':
			second = A;
			break;
		case 'B':
			second = B;
			break;
		case 'C':
			second = C;
			break;
		default:
			throw new RuntimeException();
		}
		if (first == null) {
			throw new RuntimeException();
		}
		if (second == null) {
			throw new RuntimeException();
		}
		A.print();
		System.out.println("");
		B.print();
		if (first.size() > second.size()) {
			for (int k = 0; k < second.size(); k++) {
				x.add((Integer) first.get(0) * (Integer) second.get(k));
			}
			for (int k = 1; k < first.size(); k++) {
				h.head = null;
				R.head = null;
				for (int z = 0; z < k; z++)
					h.add(0);
				for (int m = 0; m < second.size(); m++) {
					h.add((Integer) first.get(k) * (Integer) second.get(m));
				}
				ListNode i = h.head;
				ListNode j = x.head;
				for (int y = 0; y < h.size(); y++) {
					if (j == null && i != null) {
						R.add((Integer) (h.get(y)));
						i = i.next;
					} else {
						R.add((Integer) (x.get(y)) + (Integer) (h.get(y)));
						i = i.next;
						j = j.next;
					}
				}
				x.head = null;
				for (int r = 0; r < R.size(); r++) {
					x.add(R.get(r));
				}
			}
		} else {
			for (int k = 0; k < first.size(); k++) {
				x.add((Integer) first.get(k) * (Integer) second.get(0));
			}
			for (int k = 1; k < second.size(); k++) {
				h.head = null;
				R.head = null;
				for (int z = 0; z < k; z++)
					h.add(0);
				for (int m = 0; m < first.size(); m++) {
					h.add((Integer) first.get(m) * (Integer) second.get(k));
				}
				ListNode i = h.head;
				ListNode j = x.head;
				for (int y = 0; y < h.size(); y++) {
					if (j == null && i != null) {
						R.add((Integer) (h.get(y)));
						i = i.next;
					} else {
						R.add((Integer) (x.get(y)) + (Integer) (h.get(y)));
						i = i.next;
						j = j.next;
					}
				}
				x.head = null;
				for (int r = 0; r < R.size(); r++) {
					x.add(R.get(r));
				}
			}
		}
		R = x;
		int[][] result = new int[R.size()][2];
		for (int k = 0; k < R.size(); k++) {
			result[k][0] = (Integer) R.get(k);
			result[k][1] = k;
		}
		result = reverse(result);
		return result;
	}
}