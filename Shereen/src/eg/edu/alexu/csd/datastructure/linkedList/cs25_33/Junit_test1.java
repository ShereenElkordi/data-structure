package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author omid
 *
 */
public class Junit_test1 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();
		sl.add(0);
		sl.add(1);
		sl.add(2, 2);
		sl.add(3, "l");
		sl.add("H");
		sl.add(5, "Data");
		Assert.assertEquals(0, sl.get(0));
		Assert.assertEquals(1, sl.get(1));
		Assert.assertEquals(2, sl.get(2));
		Assert.assertEquals("l", sl.get(3));
		Assert.assertEquals("H", sl.get(4));
		Assert.assertEquals("Data", sl.get(5));

		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add(0);
		dl.add(1);
		dl.add(2, 2);
		dl.add(3, "l");
		dl.add("H");
		dl.add(5, "Data");
		Assert.assertEquals(0, dl.get(0));
		Assert.assertEquals(1, dl.get(1));
		Assert.assertEquals(2, dl.get(2));
		Assert.assertEquals("l", dl.get(3));
		Assert.assertEquals("H", dl.get(4));
		Assert.assertEquals("Data", dl.get(5));
	}

}
