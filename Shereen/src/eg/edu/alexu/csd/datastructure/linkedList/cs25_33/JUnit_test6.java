package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author omid
 *
 */
public class JUnit_test6 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();
		sl.add(0);
		sl.add(1);
		sl.add(2);
		sl.add(3);
		sl.add(4);
		sl.add(5);
		sl.add(6);
		sl.add(7);
		sl.add(8);
		sl.add(9);
		sl.remove(4);
		Assert.assertEquals(9, sl.size());
		Assert.assertEquals(5, sl.get(4));

		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add(0);
		dl.add(1);
		dl.add(2);
		dl.add(3);
		dl.add(4);
		dl.add(5);
		dl.add(6);
		dl.add(7);
		dl.add(8);
		dl.add(9);
		dl.remove(4);
		Assert.assertEquals(9, dl.size());
		Assert.assertEquals(5, dl.get(4));
	}

}
