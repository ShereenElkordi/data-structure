package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

/**
 * @author omid
 *
 */
public class DoublyLinkedList implements ILinkedList {
	private NodesList head = null;

	public void add(int index, Object element) {
		NodesList newNode = new NodesList(null, element, null);
		if (index == 0) {
			newNode.next = head;
			head = newNode;
		} else if (index < 0 || index > size())
			throw null;
		else {
			NodesList i = head;
			for (int j = 0; j < index - 1; j++) {
				i = i.next;
			}
			newNode.next = i.next;
			newNode.prev = i;
			i.next = newNode;
		}
	}

	public void add(Object element) {
		if (head == null)
			head = new NodesList(null, element, null);
		else {
			NodesList current = head;
			while (current.next != null)
				current = current.next;

			NodesList newNode = new NodesList(current, element, null);
			current.next = newNode;
		}
	}

	public Object get(int index) {
		if (index < 0 || index > size())
			throw null;
		NodesList i = head;
		for (int count = 0; count < index; count++) {
			i = i.next;
		}
		Object element = i.data;
		return element;
	}

	public void set(int index, Object element) {
		if (index < 0 || index > size())
			throw null;
		NodesList i = head;
		for (int count = 0; count < index; count++) {
			i = i.next;
		}
		i.data = element;
	}

	public void clear() {
		head = null;
	}

	public void print() {
		NodesList current = head;
		while (current != null) {
			System.out.print(current.data + " ");
			current = current.next;
		}
	}

	public boolean isEmpty() {
		if (head == null)
			return true;
		return false;
	}

	public void remove(int index) {
		if (index < 0 || index > size())
			throw null;
		else if (index == 0) {
			head = head.next;
			head.prev = null;
		} else {
			NodesList current = head;
			for (int i = 0; i < index - 1; i++)
				current = current.next;
			current.next = current.next.next;
		}
	}

	public int size() {
		NodesList current = head;
		int counter = 0;
		while (current != null) {
			current = current.next;
			++counter;
		}
		return counter;
	}

	public ILinkedList sublist(int fromIndex, int toIndex) {
		if (fromIndex < 0 || fromIndex > size() || toIndex < 0 || toIndex > size())
			throw null;
		DoublyLinkedList newList = new DoublyLinkedList();
		for (int i = fromIndex; i <= toIndex; i++)
			newList.add(get(i));
		return newList;
	}

	public boolean contains(Object o) {
		NodesList current = head;
		while (current != null) {
			if (current.data.equals(o))
				return true;
			current = current.next;
		}
		return false;
	}

}
