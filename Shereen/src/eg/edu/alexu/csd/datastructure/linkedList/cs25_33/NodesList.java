package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

/**
 * @author omid
 *
 */
public class NodesList {

	public Object data;
	public NodesList next;
	public NodesList prev;

	public NodesList(NodesList prev, Object data, NodesList next) {
		this.prev = prev;
		this.data = data;
		this.next = next;
	}
}
