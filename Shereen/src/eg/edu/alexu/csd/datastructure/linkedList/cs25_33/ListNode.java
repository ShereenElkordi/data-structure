package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

/**
 * @author omid
 *
 */
public class ListNode {
	public Object data;
	public ListNode next;

	public ListNode(Object element) {
		this.data = element;
	}
}
