package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author omid
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ Junit_test1.class, JUnit_test2.class, JUnit_Test3.class, JUnit_Test4.class, JUnit_test5.class,
		JUnit_test6.class, JUnit_Test7.class, JUnit_Test8.class })
public class AllTests {

}
