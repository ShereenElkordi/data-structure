package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author omid
 *
 */
public class JUnit_Test4 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();
		sl.add("A");
		sl.add("H");
		sl.add("E");
		sl.add("K");
		sl.add("J");
		sl.set(2, "V");
		Assert.assertEquals("V", sl.get(2));

		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add("A");
		dl.add("H");
		dl.add("E");
		dl.add("K");
		dl.add("J");
		dl.set(2, "V");
		Assert.assertEquals("V", dl.get(2));
	}

}
