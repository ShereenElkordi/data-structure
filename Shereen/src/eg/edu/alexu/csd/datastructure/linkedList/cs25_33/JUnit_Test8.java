package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author omid
 *
 */
public class JUnit_Test8 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();
		sl.add(0);
		sl.add(1);
		sl.add(2);
		sl.add(3);
		sl.add(4);
		sl.clear();
		Assert.assertTrue(sl.isEmpty());

		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add(0);
		dl.add(1);
		dl.add(2);
		dl.add(3);
		dl.add(4);
		dl.clear();
		Assert.assertTrue(dl.isEmpty());
	}

}
