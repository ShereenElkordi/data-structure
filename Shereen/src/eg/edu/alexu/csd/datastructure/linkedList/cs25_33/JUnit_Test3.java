package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author omid
 *
 */
public class JUnit_Test3 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();
		sl.add(0, 10);
		sl.add(1, 4);
		sl.add(2, 55);
		sl.add(1, "R");
		sl.add(40);
		sl.add(2, 7);
		sl.add(2, 20);
		sl.add(2, 100);
		Assert.assertEquals(100, sl.get(2));

		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add(0, 10);
		dl.add(1, 4);
		dl.add(2, 55);
		dl.add(1, "R");
		dl.add(40);
		dl.add(2, 7);
		dl.add(2, 20);
		dl.add(2, 100);
		Assert.assertEquals(100, dl.get(2));
	}

}
