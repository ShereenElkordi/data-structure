package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import java.util.Scanner;

/**
 * @author omid
 *
 */
public class PolyApp {
	public static void appLoop() {
		System.out.print("Please choose an action\n1- Set a polynomial variable\n2- Print"
				+ " the value of a polynomial variable\n3- Add two polynomials\n4- "
				+ "Subtract two polynomials\n5- Multiply two polynomials\n6- "
				+ "Evaluate a polynomial at some point\n7- Clear a polynomial variable");

		Scanner s = new Scanner(System.in);
		int[][] array = new int[10][2];
		PolyFunc obj = new PolyFunc();
		int num;
		char c;
		num = s.nextInt();
		switch (num) {
		case 1:
			System.out.println("Insert the variable name : A, B or C");
			c = s.next().charAt(0);
			if (c != 'A' && c != 'B' && c != 'C') {
				extracted();
			}
			System.out.println(
					"Insert the polynomial terms in the form :\n(coeff1 , exponent1 ), (coeff2 , exponent2 ), ..");
			int i = 1;
			int j = 0;
			int k = 1;
			array[j][j] = s.nextInt();
			array[j][k] = s.nextInt();
			while (array[i - k][k] != 0) {
				j = 0;
				array[i][j] = s.nextInt();
				array[i][k] = s.nextInt();
				i++;
			} // a new array with the exact size of entries
			int[][] actArray = new int[i][2];
			for (int m = 0; m < i; m++) {
				actArray[m][0] = array[m][0];
				actArray[m][k] = array[m][k];
			}
			obj.setPolynomial(c, actArray);
			appLoop();
			break;
		case 2:
			System.out.println("Insert the variable name : A, B or C");
			c = s.next().charAt(0);
		    String res=obj.print(c);
		    System.out.println(res);
			appLoop();
			break;
		case 3:
			System.out.println("Insert first operand variable name: A, B or C");
			char poly1 = s.next().charAt(0);
			System.out.println("Insert second operand variable name: A, B or C");
			char poly2 = s.next().charAt(0);
			int[][] addResult = obj.add(poly1, poly2);
			for (int r = 0; r < addResult.length; r++) {
				System.out.print(addResult[r][0]);
				System.out.print(addResult[r][1]);
			}
			appLoop();
			break;
		case 4:
			char d;
			System.out.println("Insert first operand variable name: A, B or C");
			c = s.next().charAt(0);
			System.out.println("Insert second operand variable name: A, B or C");
			d = s.next().charAt(0);
			int[][] Result =obj.subtract(c, d);
			for (int r = 0; r < Result.length; r++) {
				System.out.print(Result[r][0]);
				System.out.print(Result[r][1]);
			}
			appLoop();
			break;
		case 5:
			c = s.next().charAt(0);
			d = s.next().charAt(0);
			obj.multiply(c, d);
			appLoop();
			break;
		case 6:
			System.out.println("Insert the variable name : A, B or C");
			c = s.next().charAt(0);
			System.out.println("Insert the constant value");
			float constValue = s.nextFloat();
			float polyValue = obj.evaluatePolynomial(c, constValue);
			System.out.println(polyValue);
			appLoop();
			break;
		case 7:
			System.out.println("Insert the variable name : A, B or C");
			c = s.next().charAt(0);
			obj.clearPolynomial(c);
			appLoop();
			break;
		default:
			extracted();
		}
		s.close();
	}

	private static void extracted() {
		throw new RuntimeException();
	}

	public static void main(String[] args) {
		appLoop();

	}

}