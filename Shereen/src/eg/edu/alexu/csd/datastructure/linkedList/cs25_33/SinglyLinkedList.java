package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

/**
 * @author omid
 *
 */
/**
 * @author omid
 *
 */
public class SinglyLinkedList implements ILinkedList {
	public ListNode head = null;

	public void add(int index, Object element) {
		ListNode NewNode = new ListNode(element);
		if (index == 0) {
			NewNode.next = head;
			head = NewNode;
		} else if (index < 0 || index > size())
			throw null;
		else {
			ListNode current = head;
			for (int i = 0; i < index - 1; i++)
				current = current.next;
			NewNode.next = current.next;
			current.next = NewNode;
		}
	}

	public void add(Object element) {
		if (head == null) {
			head = new ListNode(element);
			head.next = null;
		} else {
			ListNode current = head;
			ListNode NewNode = new ListNode(element);
			while (current.next != null)
				current = current.next;
			NewNode.next = null;
			current.next = NewNode;
		}
	}

	public Object get(int index) {
		// added because of stack implementation timed out
		if (index == 0) {
			return head.data;
		}
		if (index < 0 || index > size())
			throw null;
		ListNode current = head;
		for (int i = 0; i < index; i++)
			current = current.next;
		Object element = current.data;
		return element;
	}

	public void set(int index, Object element) {
		if (index < 0 || index > size())
			throw null;
		ListNode current = head;
		for (int i = 0; i < index; i++)
			current = current.next;
		current.data = element;
	}

	public void clear() {
		head = null;
	}

	public void print() {
		ListNode current = head;
		while (current != null) {
			System.out.print(current.data + " ");
			current = current.next;
		}
	}

	public boolean isEmpty() {
		if (head == null)
			return true;
		return false;
	}

	public void remove(int index) {
		if (index == 0)
			head = head.next;
		else if (index < 0 || index > size())
			throw null;
		else {
			ListNode current = head;
			for (int i = 0; i < index - 1; i++)
				current = current.next;
			current.next = current.next.next;
		}
	}

	public int size() {
		ListNode current = head;
		int counter = 0;
		while (current != null) {
			current = current.next;
			++counter;
		}
		return counter;
	}

	public ILinkedList sublist(int fromIndex, int toIndex) {
		if (fromIndex < 0 || fromIndex > size() || toIndex < 0 || toIndex > size())
			throw null;
		SinglyLinkedList newList = new SinglyLinkedList();
		for (int i = fromIndex; i <= toIndex; i++)
			newList.add(get(i));
		return newList;
	}

	public boolean contains(Object o) {
		ListNode i = head;
		for (i = head; i != null; i = i.next) {
			if (i.data.equals(o)) {
				return true;
			}
		}
		return false;
	}
}
