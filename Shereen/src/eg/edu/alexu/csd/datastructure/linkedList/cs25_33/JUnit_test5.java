package eg.edu.alexu.csd.datastructure.linkedList.cs25_33;

import org.junit.Assert;
import org.junit.Test;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

/**
 * @author omid
 *
 */
public class JUnit_test5 {

	@Test
	public void test() {
		SinglyLinkedList sl = new SinglyLinkedList();
		sl.add(0);
		sl.add(1);
		sl.add(2);
		sl.add(3);
		sl.add(4);
		sl.add(5);
		sl.add(6);
		sl.add(7);
		sl.add(8);
		sl.add(9);
		ILinkedList sub = sl.sublist(2, 7);
		Assert.assertEquals(6, sub.size());
		Assert.assertEquals(2, sub.get(0));
		Assert.assertEquals(3, sub.get(1));
		Assert.assertEquals(4, sub.get(2));
		Assert.assertEquals(5, sub.get(3));
		Assert.assertEquals(6, sub.get(4));
		Assert.assertEquals(7, sub.get(5));

		DoublyLinkedList dl = new DoublyLinkedList();
		dl.add(0);
		dl.add(1);
		dl.add(2);
		dl.add(3);
		dl.add(4);
		dl.add(5);
		dl.add(6);
		dl.add(7);
		dl.add(8);
		dl.add(9);
		ILinkedList subD = dl.sublist(2, 7);
		Assert.assertEquals(6, subD.size());
		Assert.assertEquals(2, subD.get(0));
		Assert.assertEquals(3, subD.get(1));
		Assert.assertEquals(4, subD.get(2));
		Assert.assertEquals(5, subD.get(3));
		Assert.assertEquals(6, subD.get(4));
		Assert.assertEquals(7, subD.get(5));
	}

}
