package eg.edu.alexu.csd.datastructure.stack.cs33;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

/**
 * @author omid
 *
 */
public class Application implements IExpressionEvaluator {

	StackImp symbols = new StackImp();
	// takes value 's' if you push an operation in stack and 'p' if you append
	// in post fix
	char lastcharwas = ' ';

	/**
	 * Takes a symbolic/numeric infix expression as input and converts it to
	 * postfix notation. There is no assumption on spaces between terms or the
	 * length of the term (e.g., two digits symbolic or numeric term)
	 * 
	 * @param expression
	 *            infix expression
	 * @return postfix expression
	 */

	public String infixToPostfix(String expression) {

		char current = ' ';
		int capacity = expression.length();
		StringBuilder postfix = new StringBuilder(capacity);
		if (expression.charAt(0) == ')') {
			throw new RuntimeException();
		}
		for (int i = 0; i < capacity; i++) {
			current = expression.charAt(i);
			if ((current >= '0' && current <= '9') || (current >= 'a' && current <= 'z')
					|| (current >= 'A' && current <= 'Z')) {
				postfix.append(current);
				postfix.append(' ');
				lastcharwas = 'p';
			} else if (current == '(') {
				symbols.push(current);
				lastcharwas = 's';
			} else if (current == '*' || current == '/') {
				if (lastcharwas == 's') {
					throw new RuntimeException();
				}
				if (symbols.isEmpty()) {
					symbols.push(current);
					lastcharwas = 's';
				} else {
					char top = (Character) symbols.peek();
					while ((top == '*' || top == '/') && (!symbols.isEmpty())) {
						postfix.append(symbols.pop());
						postfix.append(' ');
					}
					symbols.push(current);
					lastcharwas = 's';
				}
			} else if (current == '+' || current == '-') {
				if (lastcharwas == 's') {
					throw new RuntimeException();
				}
				if (symbols.isEmpty()) {
					symbols.push(current);
					lastcharwas = 's';
				} else {
					char top = (Character) symbols.peek();
					while ((top == '*' || top == '/' || top == '+' || top == '-') && (!symbols.isEmpty())) {
						postfix.append(symbols.pop());
						postfix.append(' ');
					}
					symbols.push(current);
					lastcharwas = 's';
				}
			} else {
				if (current == ')') {
					while (((Character) symbols.peek() != '(')) {
						postfix.append(symbols.pop());
						postfix.append(' ');
					}
					if (symbols.isEmpty()) {
						throw new RuntimeException();
					} else {
						symbols.pop();
					}
				}
				lastcharwas = ')';
			}
		}
		if (!symbols.isEmpty()) {
			while (!symbols.isEmpty()) {
				postfix.append(symbols.pop());
				postfix.append(' ');
			}
		}
		postfix.setLength(postfix.length() - 1);

		return postfix.toString();
	}

	/**
	 * Evaluate a postfix numeric expression, with a single space separator
	 * 
	 * @param expression
	 *            postfix expression
	 * @return the expression evaluated value
	 */

	public int evaluate(String expression) {
		StackImp stack = new StackImp();
		int length = expression.length();
		if (length == 0 || expression == null) {
			throw new RuntimeException();
		}
		int i = 0;
		while (i < length) {
			int j = i;
			while (i < length && expression.charAt(i) != ' ') {
				i++;
			}
			if (j != i) {
				String current = expression.substring(j, i);
				try {
					int num = Integer.parseInt(current);
					if(Character.getNumericValue(current.charAt(0))==-1){
						throw new RuntimeException();
					}
					stack.push(num);
				} catch (NumberFormatException e) {
					if (current.length() == 2) {
						throw new RuntimeException();
					}
					int secondOperand = (Integer) stack.pop();
					int firstOperand = (Integer) stack.pop();
					int result = 0;
					if (current.charAt(0) == '+') {
						result = firstOperand + secondOperand;
					} else if (current.charAt(0) == '-') {
						result = firstOperand - secondOperand;
					} else if (current.charAt(0) == '*') {
						result = firstOperand * secondOperand;
					} else if (current.charAt(0) == '/') {
						result = firstOperand / secondOperand;
					} else {
						throw new RuntimeException();
					}
					stack.push(result);
				}
			}
			i++;
		}
		if (stack.size() == 0) {
			throw new RuntimeException();
		}
		if (stack.size() > 1) {
			return 0;
		}
		return (Integer) stack.pop();
	}
}
