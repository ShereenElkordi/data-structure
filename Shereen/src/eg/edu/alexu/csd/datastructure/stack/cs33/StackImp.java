package eg.edu.alexu.csd.datastructure.stack.cs33;

import eg.edu.alexu.csd.datastructure.linkedList.cs25_33.SinglyLinkedList;
import eg.edu.alexu.csd.datastructure.stack.IStack;

/**
 * @author omid
 *
 */
/**
 * @author omid
 *
 */
public class StackImp implements IStack {

	SinglyLinkedList stack = new SinglyLinkedList();
	int counter = 0;

	/**
	 * Inserts a specified element at the specified position in the list.
	 * 
	 * @param index
	 *            zero-based index
	 * @param element
	 *            object to insert
	 */
	public void add(int index, Object element) {
		if (index < 0 || index > size()) {
			throw new RuntimeException();
		} else if (index == stack.size()) {
			stack.add(0, element);
			counter++;
		} else {
			stack.add(size() - index, element);
			counter++;
		}
	}

	/**
	 * Removes the element at the top of stack and returns that element.
	 * 
	 * @return top of stack element, or through exception if empty
	 */
	public Object pop() {
		if (stack.isEmpty()) {
			throw new RuntimeException();
		} else {
			Object element = stack.get(0);
			stack.remove(0);
			counter--;
			return element;
		}
	}

	/**
	 * Get the element at the top of stack without removing it from stack.
	 * 
	 * @return top of stack element, or through exception if empty
	 */

	public Object peek() {
		if (stack.isEmpty()) {
			throw new RuntimeException("expression");
		} else {
			Object element = stack.get(0);
			return element;
		}
	}

	/**
	 * Pushes an item onto the top of this stack.
	 * 
	 * @param object
	 *            to insert
	 */
	public void push(Object element) {
		stack.add(0, element);
		counter++;
	}

	/**
	 * Tests if this stack is empty
	 * 
	 * @return true if stack empty
	 */
	public boolean isEmpty() {
		if (stack.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the number of elements in the stack.
	 * 
	 * @return number of elements in the stack
	 */

	public int size() {
		return counter;
	}

}
