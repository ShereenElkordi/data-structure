package eg.edu.alexu.csd.datastructure.calculator.cs33;

import eg.edu.alexu.csd.datastructure.calculator.ICalculator;

public class functions implements ICalculator {

	public int add(int x, int y) {
		return x + y;
	}

	public float divide(int x, int y) {
		float ans = (float) x / y;
		return ans;
	}
}
