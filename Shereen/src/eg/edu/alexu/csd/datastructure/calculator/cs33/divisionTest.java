package eg.edu.alexu.csd.datastructure.calculator.cs33;

import static org.junit.Assert.*;

import org.junit.Test;

public class divisionTest {

	@Test
	public void test() {
		functions testing = new functions();
		float output = testing.divide(3, 4);
		float correct = (float) 0.75;
		assertEquals(correct, output, 0);
	}

}
