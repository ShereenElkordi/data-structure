package eg.edu.alexu.csd.datastructure.iceHockey.cs33;

import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;

import java.awt.Point;

public class functions implements IPlayersFinder {
	int maxRow = -1;
	int maxCol = -1;
	int minRow = 100;
	int minCol = 100;
	int squareNum = 0;
	int counter = 0;
	int key = 0;
	int rowSize = 0;
	int colSize = 0;
	boolean[][] array = new boolean[50][50];
	char[][] photoc = new char[50][50];
	Point[] centerPoints = new Point[50];
	Point[] empty = {};

	public java.awt.Point[] findPlayers(String[] photo, int team, int threshold) {
		counter = 0;
		key = team;
		rowSize = photo.length;
		colSize = photo[0].length();
		if (photo.length == 0) {
			return empty;
		}

		for (int k = 0; k < centerPoints.length; k++) {
			centerPoints[k] = new Point();
		}
		for (int i = 0; i < photo.length; i++) {
			for (int j = 0; j < photo[0].length(); j++) {
				array[i][j] = false;
			}
		}
		for (int i = 0; i < photo.length; i++) {
			for (int j = 0; j < photo[0].length(); j++) {
				photoc[i][j] = photo[i].charAt(j);
			}
		}
		for (int i = 0; i < photo.length; i++) {
			for (int j = 0; j < photo[0].length(); j++) {
				maxRow = -1;
				maxCol = -1;
				minRow = 100;
				minCol = 100;
				squareNum = 0;
				dfs(i, j);
				if (squareNum * 4 >= threshold) {
					centerPoints[counter].x = minCol + maxCol + 1;
					centerPoints[counter].y = minRow + maxRow + 1;
					counter++;
				}
			}
		}
		Point[] parray = new Point[counter];
		for (int k = 0; k < counter; k++) {
			parray[k] = new Point();
		}
		for (int i = 0; i < counter; i++) {
			parray[i].x = centerPoints[i].x;
			parray[i].y = centerPoints[i].y;
		}
		for (int i = 0; i < counter; i++) {
			for (int j = i; j > 0; j--) {
				if ((parray[j].x < parray[j - 1].x)
						|| (parray[j].x == parray[j - 1].x && parray[j].y < parray[j - 1].y)) {
					Point temp;
					temp = parray[j];
					parray[j] = parray[j - 1];
					parray[j - 1] = temp;
				}
			}
		}
		return parray;
	}

	void dfs(int x, int y) {
		if (x >= 0 && x < rowSize && y >= 0 && y < colSize && array[x][y] == false) {
			if (photoc[x][y] - '0' == key) {
				array[x][y] = true;
				squareNum++;
				if (x < minRow) {
					minRow = x;
				}
				if (x > maxRow) {
					maxRow = x;
				}
				if (y < minCol) {
					minCol = y;
				}
				if (y > maxCol) {
					maxCol = y;
				}
				dfs(x + 1, y);
				dfs(x, y + 1);
				dfs(x - 1, y);
				dfs(x, y - 1);
			}
		}
	}
}
